const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user.js");
const courseRoutes = require("./routes/course.js");

const app = express();

// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.c4sq4kv.mongodb.net/s37-s41?retryWrites=true&w=majority",{
	useNewUrlParser : true,
	useUnifiedTopology : true,
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

// MIDDLEWARES
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// process.env.port is to choose between local url or your own domain/url
// it will always choose your own domain/url if available
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});